Weather = function () {

    var that = this;

    this.latitude = 9.4360724;
    this.longitude = 11.0310843;

    this.$weather = $('<section id="weather">' + '<p class="weatherStatus"></p>' + '<p class="temperature"></p>' + '</section>');

    this.$weatherStatus = this.$weather.find('.weatherStatus');
    this.$temperature = this.$weather.find('.temperature');


    function init() {
        console.log('Weather Initialized');
        $('html').removeClass('no-js');
        $('body').append(that.$weather);
        getLocation();

    }

    function getLocation() {
        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function (position) {

                that.latitude = position.coords.latitude;
                that.longitude = position.coords.longitude;
                returnPosition();

            }, returnPosition);

        } else {
            //that.$weatherStatus.innerHTML = "Geolocation is not supported by this browser.";
            returnPosition();
        }
    }

    //return position
    function returnPosition() {
        console.log('Latitude: ' + that.latitude + '\n' + 'Longitude: ' + that.longitude);
        var data = that.latitude + ',' + that.longitude
        console.log( data );
        $.getJSON( 'controller.php', {
            lat: that.latitude,
            lng: that.longitude,
        }, handleServerResponse );
    }


    function handleServerResponse( response ){
        console.log('handleServerResponse checking in');
        var weatherStatus = response.data.query.results.channel.item.condition.text;
        var temperature = parseInt(response.data.query.results.channel.item.condition.temp);
        
        
        that.$weatherStatus.html( weatherStatus );
        
        that.$temperature.html( parseInt((( temperature - 32) / (1.8)))  +"&#8451; / " +temperature +"&#8457;");
        

    }

    //initialize the app on document ready (shorthand)
    $(init);

}

var weather = new Weather();