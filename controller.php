<?php
define( 'GOOGLE_GEOCODING_KEY', 'AIzaSyDRVVXdAEm5wZs37mYsKJ3KKI2a3BcEemc');
define( 'GOOGLE_GEOCODING_URL', 'https://maps.googleapis.com/maps/api/geocode/json');
define( 'YAHOO_WEATHER_URL', "https://query.yahooapis.com/v1/public/yql");

$query_string = 'latlng=' . $_GET[ 'lat' ] . "," . $_GET[ 'lng' ];
$query_string = $query_string . '&key=' . GOOGLE_GEOCODING_KEY;
$googleOptions = array(
    CURLOPT_URL => GOOGLE_GEOCODING_URL . '?' . $query_string,
    CURLOPT_RETURNTRANSFER => true
);

$curl = curl_init();
curl_setopt_array( $curl, $googleOptions );
$response = json_decode( curl_exec( $curl ) );
$status = curl_getinfo( $curl, CURLINFO_HTTP_CODE );    
if( $status == 200 ){
    // successful connection to Google Geocoding
    // extract city and district, Province/State, into
    // local variables to be used in Weather query
    $city = strtolower($response -> {'results'}[0] -> {'address_components'}[4] -> {'long_name'});
    $district = strtolower($response -> {'results'}[0] -> {'address_components'}[6] -> {'short_name'});
    
    curl_close( $curl );
    // yql(Yahoo query language) specific query string to be
    // used in order to access api
    $yql = "select item.condition from weather.forecast where woeid in (select woeid from geo.places(1) where text=\" $city, $district \")";
    $yahooOptions = array(
        CURLOPT_URL => YAHOO_WEATHER_URL . "?q=" . urlencode($yql) . "&format=json",
        CURLOPT_RETURNTRANSFER => true
    );
    
    
    $curl = curl_init();
    curl_setopt_array( $curl, $yahooOptions );
    $response = json_decode( curl_exec( $curl ) );
    $status = curl_getinfo( $curl, CURLINFO_HTTP_CODE );  
    
    if( $status == 200 ){
        curl_close( $curl );
        $jSend = array(
            'status' => 'success',
            'data' => $response
        );
    }else{
        //error occured during connection
        curl_close( $curl );
        $jSend = array(
            'status' => 'error',
            'message' => "There was a Yahoo API server issue, status code: $status"
        );
        
    }
}else{
    //error occured during connection
    curl_close( $curl );
    $jSend = array(
        'status' => 'error',
        'message' => "There was a Google API server issue, status code: $status"
    );
    
}

echo json_encode( $jSend, JSON_PRETTY_PRINT );



